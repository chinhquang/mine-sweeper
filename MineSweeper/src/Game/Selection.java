package Game;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Selection {

	private JFrame frmGameMode;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Selection window = new Selection();
					window.frmGameMode.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Selection() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGameMode = new JFrame();
		frmGameMode.setTitle("Game mode");
		frmGameMode.setBounds(100, 100, 450, 300);
		frmGameMode.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGameMode.getContentPane().setLayout(null);
		
		JButton btnChoose = new JButton("Choose");
		
		btnChoose.setBounds(167, 186, 97, 25);
		frmGameMode.getContentPane().add(btnChoose);
		
		JComboBox cbb = new JComboBox();
		cbb.setModel(new DefaultComboBoxModel(new String[] {"Easy", "Hard", "Super Hard"}));
		cbb.setFont(new Font("Tahoma", Font.BOLD, 13));
		cbb.setBounds(228, 82, 129, 31);
		frmGameMode.getContentPane().add(cbb);
		
		JLabel lblSelectGameMode = new JLabel("Select game mode");
		lblSelectGameMode.setBounds(48, 89, 107, 16);
		btnChoose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(cbb.getSelectedItem().toString().equals("Easy")){
					new MainFrame();
				}
				else if(cbb.getSelectedItem().toString().equals("Hard")){
					new MainFrame3();
				}
				else if(cbb.getSelectedItem().toString().equals("Super Hard")){
					new MainFrame2();
				}
			}
		});
		frmGameMode.getContentPane().add(lblSelectGameMode);
	}
}

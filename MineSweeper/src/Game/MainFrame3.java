package Game;

import javax.swing.*;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeListener;

public class MainFrame3 {
    //Declaration of all calculator's components.
    
    public static final int Mine =10;
    public static int count=0;
    
    public static boolean[][] checkConDiTion= new boolean[26][26];
    public static int MineCount = 45;
    public static int FlagCount = 45;
    public boolean [][] FlagCover = new boolean[26][26];
    JPanel windowContent;
    JPanel pl;
    JButton flag; 
    JTextField txtMine=new JTextField();
    JTextField txtTime;
    JPanel selection;
    JButton [][]button = new JButton[26][26];
    static int [][]a = new int [26][26];
    
    
    static void MatrixRandomGen(int a[][], int cols, int rows, int bombcount){
    	int i; int j; int s;
		for(int k =0;k<bombcount;k++)
		{
            do{
            	i=(int)(Math.random()*10000000%rows);
                j=(int)(Math.random()*10000000%rows);
            }while(a[i][j]==Mine);
            a[i][j]=Mine;
		}
    }

    //--------Set Cells around a Mine ------------------
    static void CheckArroundMine(int a[][],int cols, int rows, int i, int j){
	
	for(int i1 = -1 ; i1<= 1 ; i1++){
            if(i+i1==-1||i+i1==rows) continue;
            for(int j1 = -1 ; j1<= 1 ; j1++){
                if(j+j1==-1||j+j1==cols) continue;
                if(a[i+i1][j+j1]!= Mine) a[i+i1][j+j1]++;

            }
		}
    }
    
    //--------Set Cells Around all Mines in the board
    static void MatrixPoint(int a[][], int cols, int rows){
    	for (int i = 0; i < rows; i++){

            for (int j = 0; j < cols; j++){
            	if (a[i][j] == Mine){
                    CheckArroundMine(a,cols,rows,i,j);
                }
            }
    	}
	
    	return;
    }
    //This function is used to check the last step of cover-space recursion
    boolean checkAround(int a[][],int rows, int cols, int i, int j, boolean check [][] ){
    	int count =0;

    	for(int i1 = -1 ; i1<= 1 ; i1++){
    		if(i+i1==-1||i+i1==rows) continue;//out of board size
    		for(int j1 = -1 ; j1<= 1 ; j1++){
    			if(j+j1==-1||j+j1==cols) continue;

			
    			if(a[i+i1][j+j1]==0 && check[i+i1][j+j1]==false)count ++;

    		}

    	}
	
    	if(count ==0)return true; //last step
    	else return false;

    }
    void spreadZero(int a[][],JButton button[][], int i, int j, int rows, int cols, boolean check [][]){
	
      
        if(checkAround(a,rows,cols,i,j,check)==true){
		
            return ;
		
        }

        check[i][j] =true;
        button[i][j].setText(null);
        button[i][j].setBackground(Color.green);

	
        for(int i1 = -1 ; i1<=1 ; i1++){
            if(i+i1==-1||i+i1==rows) continue;

            for(int j1 = -1 ; j1<= 1 ; j1++){
            	if(j+j1==-1||j+j1==cols) continue;
			
            	if(a[i+i1][j+j1]!=0){
                    String filePath  = "src/Icon/"+a[i+i1][j+j1]+".png";
                    button[i+i1][j+j1].setIcon(new ImageIcon(filePath));
                    button[i+i1][j+j1].setBackground(Color.CYAN);
				
            	}
            }
	
        }
        for(int i1 = -1 ; i1<=1 ; i1++){
            if(i+i1==-1||i+i1==rows) continue;

            for(int j1 = -1 ; j1<= 1 ; j1++){
                if(j+j1==-1||j+j1==cols) continue;
			
                if(a[i+i1][j+j1]==0 && check[i+i1][j+j1]==false){
                    spreadZero(a,button,i+i1,j+j1,rows,cols,check);
                }
            }
	
        }
    }
    public void coverSpaceEvent(JButton a1,int i, int j,JButton button[][],boolean CheckConDiTion[][], int a[][])   {
        a1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                spreadZero(a, button, i, j, 15, 15, checkConDiTion);
            }
        });
        return;
    }
    public void coverCellsEvent(JButton btn, int a[][], int i, int j){
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String filePath  = "src/Icon/"+a[i][j]+".png";
                btn.setIcon(new ImageIcon(filePath));
                btn.setBackground(Color.CYAN);
            }
        });
        return;
    }
    public void MineEvent(JButton a){
        a.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                a.setIcon(new ImageIcon("src/Icon/Mine.png"));
                JOptionPane.showMessageDialog(windowContent, "You lose. Good Luck Next Time");
                
                
            }
        });
        return;
    }
    public void FlagPlacing(JButton btn, int a[][],int i, int j, JTextField txtMine,boolean FlagCover[][]){
    	btn.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				if(e.getButton()==MouseEvent.BUTTON3){
					btn.setIcon(new ImageIcon("src/Icon/Flag.png"));
					if(a[i][j]==Mine){
						if(FlagCover[i][j]==false){
							FlagCover[i][j]=true;
							MineCount--;
							FlagCount--;
							txtMine.setText(FlagCount+"");
							if(MineCount==0)JOptionPane.showMessageDialog(windowContent, "Victory");
						}else {
							FlagCover[i][j]=false;
							MineCount++;
							FlagCount++;
							txtMine.setText(FlagCount+"");
							btn.setIcon(null);
						}
					}
					if(a[i][j]!=Mine){
						if(FlagCover[i][j]==false){
							FlagCover[i][j]=true;
							FlagCount--;
							txtMine.setText(FlagCount+"");
						}else{
							FlagCover[i][j]=false;
							FlagCount++;
							txtMine.setText(FlagCount+"");
							btn.setIcon(null);
						}
					}
				}
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
    }
    
    //Constructor creates the components in memory and adds the to the frame using combination of Borderlayout.
    public MainFrame3() {
        windowContent= new JPanel();
        flag =  new JButton(new ImageIcon("src/Icon/Flag.png"));
        flag.setPreferredSize(new Dimension(50,50));
        
    // Set the layout manager for this panel
        BorderLayout bl = new BorderLayout();
        windowContent.setLayout(bl);
        
    //Set tool bar to be box layout
        selection = new JPanel();
        FlowLayout box = new FlowLayout();
        box.setHgap(30);
        selection.setLayout(box);
        
    //Add toolbar element
        selection.add(flag);
        
        
        selection.add(new JLabel("Mine : "));
        txtMine=new JTextField(); txtMine.setBackground(Color.ORANGE);
        txtMine.setEditable(false);
        txtMine.setPreferredSize(new Dimension(50,50));
        txtMine.setHorizontalAlignment(JTextField.CENTER);
        txtMine.setText(MineCount+"");
        selection.add(txtMine);
        
    //Add toolbar panel into Main Panel    
    windowContent.add("North",selection);
    pl = new JPanel ();
        GridLayout gl =new GridLayout(15,15);
        pl.setLayout(gl);
    
    //Generate buttons presenting for cells    
        for(int i = 0; i < 15; i++)
            for(int j =0;j<15;j++){
                button[i][j]=new JButton();
                
                button[i][j].setPreferredSize(new Dimension(50, 50));
                pl.add(button[i][j]);
                
            }
        
        
         
    
        
    //set value hided behine each button
    //Init hided value by integers
        for(int i = 0 ; i<15;i++){
            checkConDiTion[i] = new boolean[26];FlagCover[i]= new boolean[26];
            for(int j = 0; j<15;j++){
                
                checkConDiTion[i][j]=false;
                FlagCover[i][j]=false;
            }
        }
        
        for(int i = 0 ; i<15;i++){
            a[i] = new int[26];
            for(int j = 0; j<15;j++){
                
                a[i][j]=0;
            }
        }
        MatrixRandomGen(a, 15, 15, MineCount);
        MatrixPoint(a, 15, 15);
        
    //Set Event for each button
        
    
        for(int i = 0 ; i < 15; i++){
            for(int j = 0; j<15; j++){
                
                    if(a[i][j]==Mine){
                        MineEvent(button[i][j]);
                    }else if(a[i][j]==0){
                        coverSpaceEvent(button[i][j], i, j, button, checkConDiTion, a);
                        
                    }else if(a[i][j]!=0){
                        coverCellsEvent(button[i][j], a, i, j);
                    }
                               
                
            }
        }
        
        for(int i = 0 ; i < 15; i++){
            for(int j = 0; j<15; j++){
            	
            	FlagPlacing(button[i][j],a,i,j,txtMine,FlagCover);
            }
        }
        
    //Add the panel pl to the center area of the window
        windowContent.add("Center",pl);
    //Create the frame and set its content pane
        JFrame frame = new JFrame("MineSweeper Easy mode");
        frame.setContentPane(windowContent);
    //set the size of the window to be big enough to accomodate all controls.
        frame.pack();
    //Finnaly, display the window
        frame.setVisible(true);
        frame.setResizable(true);
    }
    

    
}
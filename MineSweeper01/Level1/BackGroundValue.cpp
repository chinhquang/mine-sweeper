#include "lib.h"
#include <stdio.h>
#include <conio.h>

void setValueForTile(char syms[maxRows][maxCols], int &rows, int &cols, char *fileinput){
	FILE * p  = fopen(fileinput,"r+");char c[2];
	fscanf(p,"%d",&rows);
	fscanf(p,"%[' ']",c);
	fscanf(p,"%d",&cols);
	fscanf(p,"%[\n]",c);
	for(int j = 0; j<rows; j++){
		for(int i = 0; i<cols; i++){

			fscanf(p,"%c",&syms[i][j]);

			fscanf(p,"%[' ']",c);
			
		}
		fscanf(p,"%[\n]",c);
	}fclose(p);

}

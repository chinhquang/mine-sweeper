#include "lib.h"

void SelectGameMode(int xpos, int ypos){
	SetColor(11);
	//set position of menu game 
	gotoxy(xpos-5,ypos+1);
	printf("Choose selection");
	gotoxy(xpos,ypos+2);
	printf("Start");
	gotoxy(xpos,ypos+3);
	printf("About");
	gotoxy(xpos,ypos+4);
	printf("Instruction");
	gotoxy(xpos,ypos+5);
	printf("Exit");
	int xCur= xpos-7;
	int yCur= ypos+2;
	gotoxy(xCur,yCur);
	printf("->");
	int c;
	do{
		c = getch();
		if(c==115 ){
			gotoxy(xCur, yCur);
			printf("  ");
			if(yCur == ypos+5){
				yCur = ypos+2;
				gotoxy(xCur, yCur);
				printf("->");
			}else {
				yCur ++;
				gotoxy(xCur, yCur);
				printf("->");
			}
		}
		if(c==119){
			gotoxy(xCur, yCur);
			printf("  ");
			if(yCur == ypos+2){
				yCur = ypos+5;
				gotoxy(xCur, yCur);
				printf("->");
			}else {
				yCur --;
				gotoxy(xCur,yCur);
				printf("->");
			}
		}
	}while(c==115||c==119);
	
}
int countMine(char syms[maxRows][maxCols], int rows, int cols){
	int count=0;
	for(int i =0 ; i<rows; i++){
		for(int j =0 ; j <cols; j++){
			if(syms[i][j] == '*') count ++;
		}
	}
	return count;
}
void drawTable(){
	ResetConsoleScreen();
	FILE *p = fopen("./../Level1/table.txt","r");
	char *buf = new char[maxLengthOfBuff];
	SetColor(15);
	if(p==NULL){
		printf("Cannot load table");
	}else {
		while(!feof(p)){
			fgets(buf,maxLengthOfBuff-1,p);
			printf("%s",buf);

		}
	}
	fclose(p);
	return ;


}


void moveEvent(int &xpos, int &ypos, char syms[maxRows][maxCols],int rows, int cols, int mineCount){
SetInitLocation:
	int flag = mineCount;
	gotoxy(2,1);
	xpos = 2;
	ypos = 1;
	while(ypos <= 9 && xpos <= 18 && ypos >= 1 && xpos>=2 ){
KeystrokeEvent:

		char c = getch();
		if(c=='w'){
			ypos--;
			if(ypos==0)ypos+=9;
			gotoxy(xpos, ypos);

		}else if(c=='s'){
			ypos++;
			if(ypos==10)ypos-=9;
			gotoxy(xpos, ypos);
		}else if(c=='a'){
			xpos-=2;
			if(xpos<2) xpos+=18;
			gotoxy(xpos, ypos);
		}
		else if(c=='d'){
			xpos+=2;
			if(xpos>18) xpos-=18;
			gotoxy(xpos, ypos);
		}
		else if(c=='l'){
			//find gold
			if(syms[ypos-1][(xpos/2)-1]=='*'){
				gotoxy(xpos,ypos);SetColor(55);
				printf("*");
				gotoxy(25,5);
				printf("LOSE - Good Luck Next Time");
				gotoxy(25,6);
				printf("Press R to  restart new game, Press E to exit " );
				char t = getch();
				if(t =='r'||t =='R'){
					return;
				}
				else if(t=='e'||t=='E'){
					system("cls");
					SetColor(15);
					exit(0);
					
				}
			}else{
				gotoxy(xpos,ypos);SetColor(55);
				printf("%c",syms[ypos-1][(xpos/2)-1]);
			}
			
			goto KeystrokeEvent;
		}
		else if(c=='r'){
			//flag
			//flag
			if(flag==0){

				goto KeystrokeEvent;
			}
			if(syms[ypos-1][(xpos/2)-1]!='*'){
				gotoxy(xpos,ypos);SetColor(55);
				printf("!");
				flag--;
			
				gotoxy(32,4);
				printf("%d",flag);

			}
			else if(syms[ypos-1][(xpos/2)-1]=='*'){
				gotoxy(xpos,ypos);SetColor(55);
				printf("!");
				flag--;
				mineCount--;
				gotoxy(32,4);
				printf("%d",flag);

			}
			if(mineCount==0){

				gotoxy(25,5);
				printf("--Victory--");
				gotoxy(25,6);
				printf("Press P to  restart new game, Press E to exit " );
				char t = getch();
				if(t =='p'||t =='P'){
					return;
				}
				else if(t=='e'||t=='E'){

					exit(0);
				}
			}
			goto KeystrokeEvent;
		}
		else{
			gotoxy(9,12);
			
			printf("You have just press an unusual  button\n. Are you sure to return back ??\n");
			printf("Press (y) button to resume, press yes NO (n) to exit game");
			char c1 = getch();
			if(c1 =='y'){
				ResetConsoleScreen();
				drawTable();
				goto SetInitLocation;
			}else if(c1=='n'){
				exit(0);
			}
		}
	}
}

void main(int argc,char *argv[])
{
	ResetConsoleScreen();
ResumeMainGameBoard:

	Title(); int a[maxRows][maxCols]={0}; char syms[maxRows][maxCols];


	int xpos = wherex()-25;
	int ypos = wherey()+2;
	int setRows,setCols;
	int rows=0;
	int cols=0;
	
	SelectGameMode(xpos,ypos);

	setValueForTile(syms, rows, cols,argv[2]);

	
	int mineCount = countMine(syms,rows,cols);
	if(wherey()==ypos+2){
	
		system("cls");
		SetColor(15);
		bool check = true;
		drawTable();
		gotoxy(25,4);
		printf ("Mine : %d", mineCount);
		moveEvent(xpos,ypos,syms,rows,cols, mineCount);
		ResetConsoleScreen();

		goto ResumeMainGameBoard;
	}
	else if(wherey()==ypos+3){
		AboutUs();
		
		char c = getch();
		if(c=='b'||c=='B') {
			ResetConsoleScreen();
			goto ResumeMainGameBoard;
		}
		else {
			system("cls");
			SetColor(15);
			exit(0);
		}
	}
	else if(wherey()==ypos+4){
		Instruction();
		printf("\nPress y to return to Menu.\nPress n to close this window");
		char c = getch();
		if(c=='y') {
			ResetConsoleScreen();
			goto ResumeMainGameBoard;
		}
		else {
			system("cls");
			SetColor(15);
			exit(0);
		}
	}

	_getch();
}

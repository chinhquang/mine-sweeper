#include <conio.h>
#include <stdio.h>
#include <dos.h>
#include <Windows.h>
#include <time.h>
#include <WinUser.h>
#define Mine 10
#define maxLengthOfBuff 255
#define maxCols 50
#define maxRows 50

#define Rows 9
#define Cols 9

void gotoxy(int x, int y);
int wherex();
int wherey();

void SetColor(int ForgC);
void Title();

void ResetConsoleScreen();
void Instruction();
void AboutUs();
void setValueForTile(char syms[maxRows][maxCols], int &rows, int &cols, char *fileinput);


void SelectGameMode(int xpos, int ypos);

void drawTable();

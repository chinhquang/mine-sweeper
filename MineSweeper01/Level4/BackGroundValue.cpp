#include "lib.h"
//This fiunction below is used to get board size, height and width from input.txt
//User can change board size via configuring input.txt
//The input file 's first line is the number of rows
void getSize(int &rows, int &cols, int &bombCount){
	FILE *p = fopen("./../Level4/input.txt","r+");
	fscanf(p,"%d",&rows);
	char c = fgetc(p);
	fscanf(p,"%d",&cols);
	c = fgetc(p);
	fscanf(p,"%d",&bombCount);


}

//---------Generate Mine randomly------------------
void MatrixRandomGen(int a[maxRows][maxCols], int cols, int rows, int bombcount){
	int i; int j; int s;
	srand(time(NULL));
	
	for(int k =0;k<bombcount;k++)
	{
		do{
			i=rand()%rows;
			j=rand()%cols;
		}while(a[i][j]==Mine);
		a[i][j]=Mine;
	}

}

//--------Set Cells around a Mine ------------------
void CheckArroundMine(int a[maxRows][maxCols],int cols, int rows, int i, int j){
	
	for(int i1 = -1 ; i1<= 1 ; i1++){
		if(i+i1==-1||i+i1==rows) continue;
		for(int j1 = -1 ; j1<= 1 ; j1++){
			if(j+j1==-1||j+j1==cols) continue;
			if(a[i+i1][j+j1]!= Mine) a[i+i1][j+j1]++;

		}

	}

}

//--------Set Cells Around all Mines in the board
void MatrixPoint(int a[maxRows][maxCols], int cols, int rows){
	for (int i = 0; i < rows; i++){

		for (int j = 0; j < cols; j++){
			if (a[i][j] == Mine){
				CheckArroundMine(a,cols,rows,i,j);
			}
		}
	}
	
	return ;
}
bool checkAround(char syms[maxRows][maxCols],int rows, int cols, int i, int j, bool check [maxRows][maxCols] ){
	int count =0;

	for(int i1 = -1 ; i1<= 1 ; i1++){
		if(i+i1==-1||i+i1==rows) continue;//out of board size
		for(int j1 = -1 ; j1<= 1 ; j1++){
			if(j+j1==-1||j+j1==cols) continue;

			
			if(syms[i+i1][j+j1]=='-' && check[i+i1][j+j1]==false)count ++;

		}

	}
	
	if(count ==0)return true; //last step
	else return false;

}
void spreadZero(char syms[maxRows][maxCols], int i, int j, int rows, int cols, bool check [maxRows][maxCols]){//i = ypos - 1; j = xpos/2-1
	
	if(checkAround(syms,rows,cols,i,j,check)==true){
		
		return ;
		
	}

	check[i][j] =true;
	gotoxy(2*j+2,i+1);
	printf("%c",syms[i][j]);

	
	for(int i1 = -1 ; i1<=1 ; i1++){
		if(i+i1==-1||i+i1==rows) continue;

		for(int j1 = -1 ; j1<= 1 ; j1++){
			if(j+j1==-1||j+j1==cols) continue;
			
			if(syms[i+i1][j+j1]!='-'){
				gotoxy(2*(j+j1)+2,i+i1+1);
				printf("%c",syms[i+i1][j+j1]);
				
			}
		}
	
	}
	for(int i1 = -1 ; i1<=1 ; i1++){
		if(i+i1==-1||i+i1==rows) continue;

		for(int j1 = -1 ; j1<= 1 ; j1++){
			if(j+j1==-1||j+j1==cols) continue;
			
			if(syms[i+i1][j+j1]=='-' && check[i+i1][j+j1]==false){
				spreadZero(syms,i+i1,j+j1,rows,cols,check);
			}
		}
	
	}
	
	return;

}
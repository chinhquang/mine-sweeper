#include "lib.h"
#include <thread>
void main(int argc, char *argv[]);
int countMine(char syms[maxRows][maxCols], int rows, int cols){
	int count=0;
	for(int i =0 ; i<rows; i++){
		for(int j =0 ; j <cols; j++){
			if(syms[i][j] == '*') count ++;
		}
	}
	return count;
}
void SelectGameMode(int xpos, int ypos){
	SetColor(11);
	//set position of menu game 
	gotoxy(xpos-5,ypos+1);
	printf("Choose selection");
	gotoxy(xpos,ypos+2);
	printf("Start");
	gotoxy(xpos,ypos+3);
	printf("About");
	gotoxy(xpos,ypos+4);
	printf("Instruction");
	gotoxy(xpos,ypos+5);
	printf("Exit");
	int xCur= xpos-7;
	int yCur= ypos+2;
	gotoxy(xCur,yCur);
	printf("->");
	int c;
	do{
		c = _getch();
		if(c==115 ){
			gotoxy(xCur, yCur);
			printf("  ");
			if(yCur == ypos+5){
				yCur = ypos+2;
				gotoxy(xCur, yCur);
				printf("->");
			}else {
				yCur ++;
				gotoxy(xCur, yCur);
				printf("->");
			}
		}
		if(c==119){
			gotoxy(xCur, yCur);
			printf("  ");
			if(yCur == ypos+2){
				yCur = ypos+5;
				gotoxy(xCur, yCur);
				printf("->");
			}else {
				yCur --;
				gotoxy(xCur,yCur);
				printf("->");
			}
		}
	}while(c==115||c==119);
	
}
void setSymbolMatrix(int a[maxRows][maxCols], char syms[maxRows][maxCols], int cols, int rows){
	//Set random Game board 's component
	for(int i = 0; i < cols; i++)
		for(int j = 0; j<rows;j++)
		{
			if(a[i][j] ==Mine) syms[i][j] = '*';// The cells have mines will be symboled as *
			else if(a[i][j] == 0) syms[i][j] = '-';
			else {
				if(a[i][j]==1) syms[i][j]='1';//1-point cells
				if(a[i][j]==2) syms[i][j]='2';//2-point cells
				if(a[i][j]==3) syms[i][j]='3';//3-point cells
				if(a[i][j]==4) syms[i][j]='4';//4-point cells
				if(a[i][j]==5) syms[i][j]='5';//1-point cells
			}
		}
}
void drawTable(int rows, int cols){
	ResetConsoleScreen();
	if(rows>=10 ||cols>=10){
		printf(" ");
		for(int i = 1 ; i <= cols; i++)
		{
			int a = i;
			while(a>=10)a-=10;
			printf("|%d",a);
		}
		printf("|\n");
		for(int i = 1 ; i <= rows; i++){
			int a = i;
			while(a>=10)a-=10;
			printf("%d",a);
			
			for(int j = 1 ; j <= cols; j++){
				printf("|_");

			}printf("|\n");
		}
	}else {

		printf(" ");
		for(int i = 1 ; i <= cols; i++)printf("|%d",i);
		printf("|\n");
		for(int i = 1 ; i <= rows; i++){
			printf("%d",i);
			for(int j = 1 ; j <= cols; j++){
				printf("|_");

			}printf("|\n");
		}
	}
}
void moveEvent(int &xpos, int &ypos, char syms[maxRows][maxCols],int rows, int cols, int mineCount,bool check[maxRows][maxCols],bool isFlag[maxRows][maxCols], HANDLE a1,char*fileTime, char *fileinput, char *player, int timeCount, int argc, char *argv[]){
SetInitLocation:

	gotoxy(2,1); int flag = mineCount;
	xpos = 2;
	ypos = 1;
	int time = 0;
	
	
	while(ypos <= rows && xpos <= cols*2 && ypos >= 1 && xpos>=2 ){
KeystrokeEvent:
		
		char c = _getch();
		if(c=='w'){
			ypos--;
			if(ypos==0)ypos+=rows;
			gotoxy(xpos, ypos);

		}else if(c=='s'){
			ypos++;
			if(ypos==rows+1)ypos-=rows;
			gotoxy(xpos, ypos);
		}else if(c=='a'){
			xpos-=2;
			if(xpos<2) xpos+=cols*2;
			gotoxy(xpos , ypos);
		}
		else if(c=='d'){
			xpos+=2;
			if(xpos>cols*2) xpos-=cols*2;
			gotoxy(xpos, ypos);
		}
		else if(c=='l'){
			//find gold
			if(syms[ypos-1][(xpos/2)-1]=='*'){
				
				gotoxy(xpos,ypos);SetColor(55);
				printf("*");
				SuspendThread(a1);
				gotoxy(45,5);
				printf("LOSE - Good Luck Next Time");
				
				gotoxy(50, 6);
				printf("Press P to return to menu.");
				gotoxy(50, 7);
				printf("Click X at the top-right corner of the program to Quit");
				char t;
				do{
					t = _getch();
				}while(t!='P'&& t!='p'&&t!='e'&&t!='E');
				if(t =='P'||t =='p'){
					ResetConsoleScreen();
					
					main(argc, argv);
					
					return ;
				}
				
				
			}else if(syms[ypos-1][(xpos/2)-1]=='-'){
				gotoxy(xpos,ypos);
				spreadZero(syms,ypos-1,(xpos/2)-1,rows,cols,check);
			}else{
				gotoxy(xpos,ypos);
				printf("%c",syms[ypos-1][(xpos/2)-1]);

			}
			
			goto KeystrokeEvent;
		}
		else if(c=='r'){
			//flag
			if(flag==0){

				goto KeystrokeEvent;
			}
			
			if(syms[ypos-1][(xpos/2)-1]!='*'){
				
				if(isFlag[ypos-1][(xpos/2)-1]==false){
					gotoxy(xpos,ypos);
					printf("!");
					flag--;
					
					isFlag[ypos-1][(xpos/2)-1]=true;
				}else{
					gotoxy(xpos,ypos);
					printf("_");
					flag++;
					isFlag[ypos-1][(xpos/2)-1]=false;
				}
				
			
				gotoxy(52,4);
				if(flag>=10)
					printf("%d",flag);
				else{
					printf("0%d",flag);

				}

			}
			else if(syms[ypos-1][(xpos/2)-1]=='*'){
				if(isFlag[ypos-1][(xpos/2)-1]==false){
					gotoxy(xpos,ypos);
					printf("!");
					flag--;
					mineCount--;
					isFlag[ypos-1][(xpos/2)-1]=true;

				}else{
					gotoxy(xpos,ypos);
					printf("_");
					flag++;
					mineCount++;
					isFlag[ypos-1][(xpos/2)-1]=false;
				}
				
				gotoxy(52,4);
				if(flag>=10)
					printf("%d",flag);
				else{
					printf("0%d",flag);

				}

			}
			if(mineCount==0){

				gotoxy(50,5);
				SuspendThread(a1);
				printf("--Victory--");
				FILE *p = fopen(fileTime, "r+");
				fscanf(p, "%d", &timeCount);
				fclose(p);
				FILE *p1 = fopen(fileinput, "a+");
				fprintf(p1, "\n%s-%d", player, timeCount);
				fclose(p1);
				gotoxy(50,6);
				printf("Press P to return to menu.");
				gotoxy(50, 7);
				printf("Click X at the top-right corner of the program to Quit" );
				char t = _getch();
				if(t =='p'||t =='P'){
					ResetConsoleScreen();
					main(argc, argv);
					return;
				}
				
			}
			goto KeystrokeEvent;
		}
		else{
			continue;
		}
		
	}
}
void playeraccount(char *player, char *fileinput) {
	system("cls");
	gotoxy(0, 2);
	printf("list player recently\n");
	printf("Name  time(s)\n");
	FILE *p = fopen(fileinput, "r+");
	char tmp[255]; char name[255]; int time = 0; 
	fscanf(p, "%[^)]", tmp);
	char c = fgetc(p);

	while (!feof(p)) {
		fscanf(p, "%[^-]", name);
		printf("%s\t", name);
	
		
		c = fgetc(p);
		fscanf(p, "%d", &time);
		printf("%d\n", time);
	}



	gotoxy(0, 0);
	printf("player 's name : ");
	fgets(player, 25, stdin);
	player[strlen(player) - 1] = 0;
	system("cls");

}
void main(int argc, char *argv[]){
	ResetConsoleScreen();
ResumeMainGameBoard:
	Title();
	int a[maxRows][maxCols]={0};
	char syms[maxRows][maxCols];
	bool check[maxRows][maxCols];
	bool isFlag[maxRows][maxCols];
	char *player = new char[26];
	char *fileinput = argv[2];
	char *fileTime = "./../Level4/time.txt";
	int cols =0;
	int rows=0;
	int bombCount =0;
	int timeCount = 0;
	getSize(rows, cols,bombCount);
	int xpos = wherex()-25;
	int ypos = wherey()+2;
	int setRows,setCols;
	SelectGameMode(xpos,ypos);
	if(wherey()==ypos+2){
		playeraccount(player,argv[2]);
		system("cls");
		SetColor(15);
		MatrixRandomGen(a,rows,cols,bombCount);
		MatrixPoint(a,rows,cols);
		setSymbolMatrix(a,syms,rows,cols);

		//--------Flag---------
		for(int i = 0; i< rows; i++)
		for(int j = 0 ; j < cols; j++){
			check [i][j] = false;

		}
		//--------Check that cell is hided or not----------
		for(int i = 0; i< rows; i++)
		for(int j = 0 ; j < cols; j++){
			isFlag [i][j] = false;//all cells are  hided=> init false
		}


		//---------------------
		gotoxy(0,15);
		
		int mineCount = countMine(syms,rows,cols);
		drawTable(rows,cols);
		gotoxy(45,4);
		printf ("Mine : %d", mineCount);
		thread b(bind(&Timer, ref(timeCount),fileTime,player)); 
		HANDLE b1 = b.native_handle();
		thread a(bind(&moveEvent, ref(xpos), ref(ypos), syms, rows, cols, mineCount, check, isFlag, b1,fileTime,fileinput,player,timeCount, argc, argv ));
		a.join();
		b.join();
		
		ResetConsoleScreen();

		goto ResumeMainGameBoard;
	}
	else if(wherey()==ypos+3){
		AboutUs();
		
		char c = getch();
		if(c=='b'||c=='B') {
			ResetConsoleScreen();
			goto ResumeMainGameBoard;
		}
		else exit(0);
	}
	else if(wherey()==ypos+4){
		Instruction();
		printf("\nPress y to return to Menu.\nPress n to close this window");
		char c = getch();
		if(c=='y') {
			ResetConsoleScreen();
			goto ResumeMainGameBoard;
		}
		else exit(0);
	}

	_getch();
}

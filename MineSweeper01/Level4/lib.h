#define _CRT_SECURE_NO_WARNINGS
#include <thread>
#include <conio.h>
#include <stdio.h>
#include <dos.h>
#include <Windows.h>
#include <time.h>
#include <WinUser.h>
#include <MMSystem.h>
using namespace std;

#define maxLengthOfBuff 255
#define maxCols 50
#define maxRows 50
#define Mine 10
#define Rows 9
#define Cols 9
void gotoxy(int x, int y);
int wherex();
int wherey(); 
void SetColor(int ForgC);
void Title();

void ResetConsoleScreen();
void Instruction();

void AboutUs();

void MatrixRandomGen(int a[maxRows][maxCols], int cols, int rows, int bombcount);
void CheckArroundMine(int a[maxRows][maxCols],int cols, int rows, int i, int j);
void MatrixPoint(int a[maxRows][maxCols], int cols, int rows);
void getSize(int &rows, int &cols, int &bombCount);

void SelectGameMode(int xpos, int ypos);
void setSymbolMatrix(int a[30][30], char syms[30][30], int cols, int rows);

bool checkAround(char syms[maxRows][maxCols],int rows, int cols, int i, int j, bool check [maxRows][maxCols] );

void spreadZero(char syms[maxRows][maxCols], int i, int j, int rows, int cols, bool check [maxRows][maxCols]);

void Timer(int &timeCount, char *fileTime, char *player);

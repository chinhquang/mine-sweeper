#include "lib.h"
void getSize(int &rows, int &cols, char *fileinput){
	FILE *p = fopen(fileinput,"r+");
	fscanf(p,"%d",&rows);
	char c = fgetc(p);
	fscanf(p,"%d",&cols);

}
void MatrixRandomGen(int a[maxRows][maxCols], int cols, int rows, int bombcount){
	int i; int j; int s;
	srand(time(NULL));
	
	for(int k =0;k<bombcount;k++)
	{
		do{
			i=rand()%rows;
			j=rand()%cols;
		}while(a[i][j]==Mine);
		a[i][j]=Mine;
	}

}
void CheckArroundMine(int a[maxRows][maxCols],int cols, int rows, int i, int j){
	
	for(int i1 = -1 ; i1<= 1 ; i1++){
		if(i+i1==-1||i+i1==rows) continue;
		for(int j1 = -1 ; j1<= 1 ; j1++){
			if(j+j1==-1||j+j1==cols) continue;
			if(a[i+i1][j+j1]!= Mine) a[i+i1][j+j1]++;

		}

	}

}
void MatrixPoint(int a[maxRows][maxCols], int cols, int rows){
	for (int i = 0; i < rows; i++){

		for (int j = 0; j < cols; j++){
			if (a[i][j] == Mine){
				CheckArroundMine(a,cols,rows,i,j);
			}
		}
	}
	
	return ;
}

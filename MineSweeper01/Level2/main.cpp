#include "lib.h"
//75 la <-  77-> 72v 78^
int countMine(char syms[maxRows][maxCols], int rows, int cols){
	int count=0;
	for(int i =0 ; i<rows; i++){
		for(int j =0 ; j <cols; j++){
			if(syms[i][j] == '*') count ++;
		}
	}
	return count;
}
void SelectGameMode(int xpos, int ypos){
	SetColor(11);
	//set position of menu game 
	gotoxy(xpos-5,ypos+1);
	printf("Choose selection");
	gotoxy(xpos,ypos+2);
	printf("Start");
	gotoxy(xpos,ypos+3);
	printf("About");
	gotoxy(xpos,ypos+4);
	printf("Instruction");
	gotoxy(xpos,ypos+5);
	printf("Exit");
	int xCur= xpos-7;
	int yCur= ypos+2;
	gotoxy(xCur,yCur);
	printf("->");
	int c;
	do{
		c = getch();
		if(c==115 ){
			gotoxy(xCur, yCur);
			printf("  ");
			if(yCur == ypos+5){
				yCur = ypos+2;
				gotoxy(xCur, yCur);
				printf("->");
			}else {
				yCur ++;
				gotoxy(xCur, yCur);
				printf("->");
			}
		}
		if(c==119){
			gotoxy(xCur, yCur);
			printf("  ");
			if(yCur == ypos+2){
				yCur = ypos+5;
				gotoxy(xCur, yCur);
				printf("->");
			}else {
				yCur --;
				gotoxy(xCur,yCur);
				printf("->");
			}
		}
	}while(c==115||c==119);
	
}
void setSymbolMatrix(int a[maxRows][maxCols], char syms[maxRows][maxCols], int cols, int rows){
	//Set random Game board 's component
	for(int i = 0; i < cols; i++)
		for(int j = 0; j<rows;j++)
		{
			if(a[i][j] ==Mine) syms[i][j] = '*';// The cells have mines will be symboled as *
			else if(a[i][j] == 0) syms[i][j] = '-';
			else {
				if(a[i][j]==1) syms[i][j]='1';//1-point cells
				if(a[i][j]==2) syms[i][j]='2';//2-point cells
				if(a[i][j]==3) syms[i][j]='3';//3-point cells
				if(a[i][j]==4) syms[i][j]='4';//4-point cells
				if(a[i][j]==5) syms[i][j]='5';//1-point cells
			}
		}
}
void drawTable(int rows, int cols){
	ResetConsoleScreen();

	printf(" ");
	for(int i = 1 ; i <= cols; i++)printf("|%d",i);
	printf("|\n");
	for(int i = 1 ; i <= rows; i++){
		printf("%d",i);
		for(int j = 1 ; j <= cols; j++){
			printf("|_");

		}printf("|\n");

	}

}
void moveEvent(int &xpos, int &ypos, char syms[maxRows][maxCols],int rows, int cols, int mineCount){
SetInitLocation:

	gotoxy(2,1); int flag = mineCount;
	xpos = 2;
	ypos = 1;
	while(ypos <= rows && xpos <= cols*2 && ypos >= 1 && xpos>=2 ){
KeystrokeEvent:

		char c = getch();
		if(c=='w'){
			ypos--;
			if(ypos==0)ypos+=rows;
			gotoxy(xpos, ypos);

		}else if(c=='s'){
			ypos++;
			if(ypos==rows+1)ypos-=rows;
			gotoxy(xpos, ypos);
		}else if(c=='a'){
			xpos-=2;
			if(xpos<2) xpos+=cols*2;
			gotoxy(xpos , ypos);
		}
		else if(c=='d'){
			xpos+=2;
			if(xpos>cols*2) xpos-=cols*2;
			gotoxy(xpos, ypos);
		}
		else if(c=='l'){
			//find gold
			if(syms[ypos-1][(xpos/2)-1]=='*'){
				gotoxy(xpos,ypos);SetColor(55);
				printf("*");
				gotoxy(25,5);
				printf("LOSE - Good Luck Next Time");
				gotoxy(25,6);
				printf("Press P to  restart new game, Press E to exit " );
				char t = getch();
				if(t =='P'||t =='p'){
					return;
				}
				else if(t=='e'||t=='E'){

					exit(0);
				}
			}else{
				gotoxy(xpos,ypos);SetColor(55);
				printf("%c",syms[ypos-1][(xpos/2)-1]);
			}
			
			goto KeystrokeEvent;
		}
		else if(c=='r'){
			//flag
			if(flag==0){

				goto KeystrokeEvent;
			}
			if(syms[ypos-1][(xpos/2)-1]!='*'){
				gotoxy(xpos,ypos);SetColor(55);
				printf("!");
				flag--;
			
				gotoxy(32,4);
				printf("%d",flag);

			}
			else if(syms[ypos-1][(xpos/2)-1]=='*'){
				gotoxy(xpos,ypos);SetColor(55);
				printf("!");
				flag--;
				mineCount--;
				gotoxy(32,4);
				printf("%d",flag);

			}
			if(mineCount==0){

				gotoxy(25,5);
				printf("--Victory--");
				gotoxy(25,6);
				printf("Press P to  restart new game, Press E to exit " );
				char t = getch();
				if(t =='p'||t =='P'){
					return;
				}
				else if(t=='e'||t=='E'){

					exit(0);
				}
			}
			goto KeystrokeEvent;
		}
		else{
			gotoxy(9,12);
			
			printf("You have just press an unusual  button\n. Are you sure to return back ??\n");
			printf("Press (y) button to resume, press yes NO (n) to exit game");
			char c1 = getch();
			if(c1 =='y'){
				ResetConsoleScreen();
				drawTable(9,9);
				goto SetInitLocation;
			}else if(c1=='n'){
				exit(0);
			}
		}
	}
}
void printdemo(char syms [maxRows][maxCols], int cols, int rows){

	for(int i = 0; i <rows; i++){
		for(int j = 0;j<cols;j++){
			printf("%c\t",syms[i][j]);
		}
		printf("\n");
	}
}
void main(int argc,char *argv[]){
	ResetConsoleScreen();
ResumeMainGameBoard:

	Title(); int a[maxRows][maxCols]={0}; char syms[maxRows][maxCols];
	int rows=0;
	int cols =0;
	
	getSize(rows, cols,argv[2] );
	int xpos = wherex()-25;
	int ypos = wherey()+2;
	int setRows,setCols;


	SelectGameMode(xpos,ypos);

	if(wherey()==ypos+2){
		
		system("cls");
		SetColor(15);

		MatrixRandomGen(a,rows,cols,9);
		MatrixPoint(a,rows,cols);
		setSymbolMatrix(a,syms,rows,cols);

		int mineCount = countMine(syms,rows,cols);
		
		
		drawTable(rows,cols);
		gotoxy(25,4);
		printf ("Mine : %d", mineCount);
		moveEvent(xpos,ypos,syms,rows,cols,mineCount);
		ResetConsoleScreen();

		goto ResumeMainGameBoard;
	}
	else if(wherey()==ypos+3){
		AboutUs();
		
		char c = getch();
		if(c=='b'||c=='B') {
			ResetConsoleScreen();
			goto ResumeMainGameBoard;
		}
		else exit(0);
	}
	else if(wherey()==ypos+4){
		Instruction();
		printf("\nPress y to return to Menu.\nPress n to close this window");
		char c = getch();
		if(c=='y') {
			ResetConsoleScreen();
			goto ResumeMainGameBoard;
		}
		else exit(0);
	}

	_getch();
}

#include "lib.h"
void gotoxy(int x, int y)
{
	static HANDLE h = NULL;  
	if(!h)
    h = GetStdHandle(STD_OUTPUT_HANDLE);
	 COORD c = { x, y };  
	SetConsoleCursorPosition(h,c);
}
int wherex()
{
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
    return csbi.dwCursorPosition.X;
}
int wherey()
{
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
    return csbi.dwCursorPosition.Y;
}
void Title(){
	FILE *p = fopen("./../Level3/Logo.txt","r+");
	char *buf = new char[1000];
	SetColor(25);
	while(!feof(p)){
		
		fgets(buf,255,p);
		
		printf("%s",buf);
	}
	return;

}
void SetColor(int ForgC)
{
	WORD wColor;
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbi;

                       //We use csbi for the wAttributes word.
	if(GetConsoleScreenBufferInfo(hStdOut, &csbi))
	{
                 //Mask out all but the background attribute, and add in the forgournd color
      wColor = (csbi.wAttributes & 0xF0) + (ForgC & 0x0F);
      SetConsoleTextAttribute(hStdOut, wColor);
	}
	return;
}

void ResetConsoleScreen(){
	system("cls");

}